import * as React from 'react';
import BackButton from "../../components/backButton/BackButton";
import CategoryCard from "../../components/categoryCard/CategoryCard";
import "./Categories.css"
import ProductsTitleAndSearch from "../../components/productsTitleAndSearch/ProductsTitleAndSearch";

export default function Categories() {

    // GET the categories from API
    const [categories, setCategories] = React.useState([
        {label: "Fruits", image:"https://cdn.pixabay.com/photo/2015/12/30/11/57/fruits-1114060_960_720.jpg"},
        {label: "Légumes", image:"https://cdn.pixabay.com/photo/2016/08/11/08/04/vegetables-1584999_960_720.jpg"},
        {label: "Viandes", image:"https://cdn.pixabay.com/photo/2015/03/03/22/54/meat-658029_960_720.jpg"},
        {label: "Poissons", image:"https://cdn.pixabay.com/photo/2016/03/05/19/02/salmon-1238248_960_720.jpg"}
    ])

    return (
        <div>
            <BackButton />
            <ProductsTitleAndSearch />
            <h2>
                Catégories
            </h2>
            <div className={'categoryList'}>
                {categories.map((category) => (
                    <CategoryCard
                        category={category}
                    />
                ))}
            </div>
        </div>
    )
}