import * as React from 'react';
import Button from '@mui/material/Button';
import logo from '../../assets/logo/logo.png';
import "./Home.css";

export default function Home() {

    return (
        <div>
            <img className={'logo'} src={logo}/>
            <h1>
                Bienvenue
            </h1>
            <p>Manger, c’est bien.<br/>
                Manger local, c’est mieux</p>
            <Button
                href={"/catégories"}
                variant="contained"
            >
                Les catégories
            </Button>
            <Button
                href={"/produits"}
                variant="contained"
            >
                Les produits
            </Button>
        </div>
    )
}