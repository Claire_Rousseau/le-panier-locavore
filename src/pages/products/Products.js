import * as React from 'react';
import BackButton from "../../components/backButton/BackButton";
import ProductCard from "../../components/productCard/ProductCard";
import ProductsTitleAndSearch from "../../components/productsTitleAndSearch/ProductsTitleAndSearch";

export default function Products() {

    // GET the products for the chosen categories from API
    const [products, setProducts] = React.useState([
        {id: "1", label: "Fruits rouges", image:"https://cdn.pixabay.com/photo/2010/12/13/10/05/berries-2277_960_720.jpg", description: "Description du produit", quantity: "200g", price: "10€"},
        {id: "1", label: "Abricots", image:"https://cdn.pixabay.com/photo/2018/05/27/16/19/apricots-3433818_960_720.jpg", description: "Description du produit", quantity: "1kg", price: "10€"}
    ])

    return (
        <div>
            <BackButton
                path={"/catégories"}
                label={"Les catégories"}
            />
            <ProductsTitleAndSearch />
            <h2>
                Fruits
            </h2>
            <div>
                {products.map((product) => (
                    <ProductCard
                        product={product}
                    />
                ))}
            </div>
        </div>

    )
}