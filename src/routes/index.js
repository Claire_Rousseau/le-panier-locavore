import { useRoutes } from 'react-router-dom';
import Home from "../pages/home/Home";
import Categories from "../pages/categories/Categories";
import Products from "../pages/products/Products";

export default function Router() {
    return useRoutes([
        { path: '', element: <Home /> },
        { path: 'catégories', element: <Categories /> },
        { path: 'produits', element: <Products /> },
    ])
}