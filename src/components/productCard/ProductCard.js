import * as React from 'react';
import "./ProductCard.css";
import {Card, CardActionArea} from "@mui/material";

export default function ProductCard(props) {
    const product = props.product;

    return (
        <Card className={'productCard'}>
            <CardActionArea>
                <div className={'cardArea'}>
                    <div className={"productImage"}>
                        <img src={product.image} alt={"Photo du produit"}/>
                    </div>
                    <div className={'productCardInfo'}>
                        <div className={'titleLine'}>
                            <p className={'productTitle'}>{product.label} - {product.quantity}</p>
                            <p className={'price'}>{product.price}</p>
                        </div>
                    </div>
                </div>
            </CardActionArea>
        </Card>
    )
}