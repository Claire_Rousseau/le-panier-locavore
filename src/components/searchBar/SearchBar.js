import * as React from 'react';
import SearchOutlinedIcon from '@mui/icons-material/SearchOutlined';
import {InputAdornment, TextField} from "@mui/material";

export default function SearchBar() {

    return (
        <TextField
            id="search"
            label="Rechercher un produit"
            variant="outlined"
            InputProps={{
                endAdornment: (
                    <InputAdornment position="end">
                        <SearchOutlinedIcon />
                    </InputAdornment>
                ),
            }}
        />
    )
}