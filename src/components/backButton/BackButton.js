import * as React from 'react';
import Button from '@mui/material/Button';
import ArrowCircleLeftOutlinedIcon from '@mui/icons-material/ArrowCircleLeftOutlined';

export default function BackButton(props) {
    const label = props.label;
    const path = props.path;

    return (
        <Button
            href={path? path : "/"}
            startIcon={<ArrowCircleLeftOutlinedIcon />}
        >
            {label? label : "Accueil"}
        </Button>
    )
}