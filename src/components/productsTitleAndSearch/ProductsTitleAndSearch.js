import * as React from 'react';
import SearchBar from "../searchBar/SearchBar";

export default function ProductsTitleAndSearch() {

    return (
        <div>
            <h1>
                Les produits
            </h1>
            <SearchBar />
        </div>

    )
}