import * as React from 'react';
import "./CategoryCard.css";
import {Card, CardActionArea, CardContent, CardMedia} from "@mui/material";

export default function CategoryCard(props) {
    const category = props.category;

    return (
        <Card className={'categoryCard'}>
            <CardActionArea
                className={'categoryCardArea'}
                href={"/produits"}
            >
                <CardMedia
                    component="img"
                    image={category.image}
                    alt="Photo de la catégorie"
                    className={"categoryImage"}
                />
                <CardContent>
                    <h3 className={"label"}>
                        {category.label}
                    </h3>
                </CardContent>
            </CardActionArea>
        </Card>
    )
}